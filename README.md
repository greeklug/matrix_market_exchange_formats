# A Matrix Market Exchange Format parser

The [Matrix Market](https://math.nist.gov/MatrixMarket/index.html) repository of test data for use in performance analysis of algorithms for numerical linear algebra. It contains multiple collections of test data, such as the [Harwell-Boeing](https://math.nist.gov/MatrixMarket/data/Harwell-Boeing/) and [SPARSKIT](https://math.nist.gov/MatrixMarket/data/SPARSKIT/) collections. The data are provided in [multiple text file formats](https://math.nist.gov/MatrixMarket/formats.html), including [Harwell-Boeing Exchange Format](https://math.nist.gov/MatrixMarket/formats.html#hb) and the [Matrix Market Exchange Format](https://math.nist.gov/MatrixMarket/formats.html#MMformat). We are using the Matrix Market Exchange Format due to its simplicity. More information regarding the format can be found in its [official documentation](https://math.nist.gov/MatrixMarket/reports/MMformat.ps).

This parser allows the reading a writting of files in the Matrix Market Exchange Format, and is based on the [reference implementation](https://math.nist.gov/MatrixMarket/mmio-c.html) provided by Matrix Market. This implementation currently does not support patterns.

## Using the library

You can use the library by building the library and linking your program with the resulting object file.

### Building and installing the package

The package is build using CMake. There is a default configuration script using the Ninja back-end. Configure with the command,
```bash
cmake -S . --preset default-config
```
build with the command,
```bash
cmake --build build/Release --target all
```
and install the library with the command:
```bash
cmake --build build/Release --target install
```

By default the installation directory is: `${HOME}/.local/opt/matrix-market`

### Linking with the library

- To link with the library simply link with: `${HOME}/.local/opt/matrix-market/lib64/libmatrixmarket.so`
- Header files are available in: `${HOME}/.local/opt/matrix-market/include/mmio.h`
