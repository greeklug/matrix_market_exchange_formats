/*
*   Matrix Market I/O library for ANSI C
*
*   See http://math.nist.gov/MatrixMarket for details.
*
*
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include "mmio.h"

static char* mm_typecode_to_str( MM_typecode const matcode );

/*****************************************************************************/
/* Header functions                                                          */
/*****************************************************************************/

int mm_is_valid( MM_typecode matcode )
{
  if ( ! mm_is_matrix( matcode ) )
    return 0;
  if ( mm_is_dense( matcode ) && mm_is_pattern( matcode ) )
    return 0;
  if ( mm_is_real( matcode ) && mm_is_hermitian( matcode ) )
    return 0;
  if ( mm_is_pattern( matcode ) && ( mm_is_hermitian( matcode ) || mm_is_skew( matcode ) ) )
    return 0;

  return 1;
}

int mm_read_banner( FILE* f, MM_typecode* matcode )
{
  char line[MM_MAX_LINE_LENGTH];
  char banner[MM_MAX_TOKEN_LENGTH];
  char mtx[MM_MAX_TOKEN_LENGTH];
  char crd[MM_MAX_TOKEN_LENGTH];
  char data_type[MM_MAX_TOKEN_LENGTH];
  char storage_scheme[MM_MAX_TOKEN_LENGTH];
  char *p;

  mm_clear_typecode( matcode );

  if ( fgets( line, MM_MAX_LINE_LENGTH, f ) == NULL )
    return MM_PREMATURE_EOF;

  if ( sscanf( line, "%s %s %s %s %s", banner, mtx, crd, data_type, storage_scheme ) != 5 )
    return MM_PREMATURE_EOF;

  /* convert to lower case */
  for ( p = mtx; *p != '\0'; *p = tolower(*p), p++ );
  for ( p = crd; *p != '\0'; *p = tolower(*p), p++ );
  for ( p = data_type; *p != '\0'; *p = tolower(*p), p++ );
  for ( p = storage_scheme; *p != '\0'; *p = tolower(*p), p++ );

  /* check for banner */
  if ( strncmp( banner, MatrixMarketBanner, strlen( MatrixMarketBanner ) ) != 0 )
    return MM_NO_HEADER;

  /* first field should be "mtx" */
  if ( strcmp( mtx, MM_MTX_STR ) != 0 )
    return  MM_UNSUPPORTED_TYPE;
  mm_set_matrix( matcode );

  /* second field describes whether this is a sparse matrix (in coordinate storgae) or a dense array */

  if ( strcmp( crd, MM_SPARSE_STR ) == 0 )
    mm_set_sparse( matcode );
  else if ( strcmp( crd, MM_DENSE_STR ) == 0 )
    mm_set_dense( matcode );
  else
    return MM_UNSUPPORTED_TYPE;

  /* third field */
  if ( strcmp( data_type, MM_REAL_STR ) == 0 )
    mm_set_real( matcode );
  else if ( strcmp( data_type, MM_COMPLEX_STR ) == 0 )
    mm_set_complex( matcode );
  else if ( strcmp( data_type, MM_PATTERN_STR ) == 0 )
    mm_set_pattern( matcode );
  else if ( strcmp( data_type, MM_INT_STR ) == 0 )
    mm_set_integer( matcode );
  else
    return MM_UNSUPPORTED_TYPE;

  /* fourth field */
  if ( strcmp( storage_scheme, MM_GENERAL_STR ) == 0 )
    mm_set_general( matcode );
  else if ( strcmp( storage_scheme, MM_SYMM_STR ) == 0 )
    mm_set_symmetric( matcode );
  else if ( strcmp( storage_scheme, MM_HERM_STR ) == 0 )
    mm_set_hermitian( matcode );
  else if ( strcmp( storage_scheme, MM_SKEW_STR ) == 0 )
    mm_set_skew( matcode );
  else
    return MM_UNSUPPORTED_TYPE;

  return 0;
}

int mm_read_mtx_crd_size( FILE* f, int* M, int* N, int* nz )
{
  char line[MM_MAX_LINE_LENGTH];
  int num_items_read;

  /* set return null parameter values, in case we exit with errors */
  *M = *N = *nz = 0;

  /* now continue scanning until you reach the end-of-comments */
  do {
    if ( fgets( line, MM_MAX_LINE_LENGTH, f ) == NULL )
      return MM_PREMATURE_EOF;
  } while ( line[0] == '%' );

  /* line[] is either blank or has M,N, nz */
  if ( sscanf( line, "%d %d %d", M, N, nz ) == 3 ) {
    return 0;
  } else {
    do {
      num_items_read = fscanf( f, "%d %d %d", M, N, nz );
      if ( num_items_read == EOF )
        return MM_PREMATURE_EOF;
    } while ( num_items_read != 3 );
  }

  return 0;
}

int mm_write_banner( FILE *f, MM_typecode const matcode )
{
  char *str = mm_typecode_to_str( matcode );
  int ret_code;

  ret_code = fprintf( f, "%s %s\n", MatrixMarketBanner, str );
  free( str );
  if ( ret_code !=2 )
    return MM_COULD_NOT_WRITE_FILE;
  else
    return 0;
}

int mm_read_mtx_array_size( FILE* f, int* M, int* N )
{
  char line[MM_MAX_LINE_LENGTH];
  int num_items_read;
  /* set return null parameter values, in case we exit with errors */
  *M = *N = 0;

  /* now continue scanning until you reach the end-of-comments */
  do {
    if (fgets( line, MM_MAX_LINE_LENGTH, f ) == NULL)
      return MM_PREMATURE_EOF;
  } while ( line[0] == '%' );

  /* line[] is either blank or has M,N, nz */
  if ( sscanf( line, "%d %d", M, N ) == 2 ) {
    return 0;
  } else { /* we have a blank line */
    do {
      num_items_read = fscanf( f, "%d %d", M, N );
      if ( num_items_read == EOF )
        return MM_PREMATURE_EOF;
    } while (num_items_read != 2);
  }

  return 0;
}

int mm_write_mtx_crd_size( FILE* f, int M, int N, int nz )
{
  if ( fprintf( f, "%d %d %d\n", M, N, nz ) != 3 )
    return MM_COULD_NOT_WRITE_FILE;
  else
    return 0;
}

int mm_write_mtx_array_size( FILE* f, int M, int N )
{
  if ( fprintf( f, "%d %d\n", M, N ) != 2 )
    return MM_COULD_NOT_WRITE_FILE;
  else
    return 0;
}

/*****************************************************************************/
/* Data functions                                                            */
/*****************************************************************************/

static void get_next_entry( int const LD, int* col, int* row, int* idx );

// use when I[], J[], and val[] are already allocated

int mm_read_mtx_crd_real_data( FILE* const f,
  int const nz,
  int* const I, int* const J, double* const val )
{
  int const idx_start = 0;
  int const n_entries = nz;

  int const ierr = mm_read_mtx_crd_real_data_range( f,
    idx_start, n_entries,
    I, J, val );

  return ierr;
}

int mm_read_mtx_crd_complex_data( FILE* const f,
  int const nz,
  int* const I, int* const J, double* const val )
{
  int const idx_start = 0;
  int const n_entries = nz;

  int const ierr = mm_read_mtx_crd_complex_data_range( f,
    idx_start, n_entries,
    I, J, val );

  return ierr;
}

int mm_read_mtx_crd_pattern_data( FILE* const f,
  int const nz,
  int* const I, int* const J )
{
  int const idx_start = 0;
  int const n_entries = nz;

  int const ierr = mm_read_mtx_crd_pattern_data_range( f,
    idx_start, n_entries,
    I, J );

  return ierr;
}

int mm_read_mtx_arr_real_data( FILE* const f,
  int const M, int const N, int const LD,
  double* const val )
{
  int const n_entries = M*N;
  int row = 0;
  int col = 0;

  int const ierr = mm_read_mtx_arr_real_data_range( f,
    M, N, LD,
    &row, &col, n_entries,
    val );

  return ierr;
}

int mm_read_mtx_arr_complex_data( FILE* const f,
  int const M, int const N, int const LD,
  double* const val )
{
  int const n_entries = M*N;
  int row = 0;
  int col = 0;

  int const ierr = mm_read_mtx_arr_complex_data_range( f,
    M, N, LD,
    &row, &col, n_entries,
    val );

  return ierr;
}

int mm_read_mtx_crd_real_data_range( FILE* const f,
  int const idx_start, int const n_entries,
  int* const I, int* const J, double* const val )
{
  int const idx_end = idx_start + n_entries;

  for ( int i = idx_start; i < idx_end; ++i ) {
    int I_i = 0;
    int J_i = 0;

    int const read = fscanf( f, "%d %d %lg", &I_i, &J_i, &val[i] );

    // ALSO ivalidates missing indices
    I[i] = I_i - 1;
    J[i] = J_i - 1;

    if ( read != 3 )
      return MM_PREMATURE_EOF;
  }

  return 0;
}

int mm_read_mtx_crd_complex_data_range( FILE* const f,
  int const idx_start, int const n_entries,
  int* const I, int* const J, double* const val )
{
  int const idx_end = idx_start + n_entries;

  for ( int i = idx_start; i < idx_end; ++i ) {
    int I_i = 0;
    int J_i = 0;

    int const read = fscanf( f, "%d %d %lg %lg", &I_i, &J_i, &val[2*i], &val[2*i+1] );
 
    // ALSO ivalidates missing indices
    I[i] = I_i - 1;
    J[i] = J_i - 1;

    if ( read != 4 )
      return MM_PREMATURE_EOF;
  }

  return 0;
}

int mm_read_mtx_crd_pattern_data_range( FILE* const f,
  int const idx_start, int const n_entries,
  int* const I, int* const J )
{
  int const idx_end = idx_start + n_entries;

  for ( int i = idx_start; i < idx_end; ++i ) {
    int I_i = 0;
    int J_i = 0;

    int const read = fscanf( f, "%d %d", &I_i, &J_i );

    // ALSO ivalidates missing indices
    I[i] = I_i - 1;
    J[i] = J_i - 1;

    if ( read != 2 )
      return MM_PREMATURE_EOF;
  }

  return 0;
}

int mm_read_mtx_arr_real_data_range( FILE* const f,
  int const M, int const N, int const LD,
  int* row, int* col, int const n_entries,
  double* const val )
{
  int idx = (*col) * LD + (*row);

  for ( int n_read = 0; n_read < n_entries; ++n_read ) { 
    int const read = fscanf( f, "%lg", &val[idx]);

    get_next_entry( LD, col, row, &idx );

    if ( read != 1 )
      return MM_PREMATURE_EOF;
  }

  return 0;
}

int mm_read_mtx_arr_complex_data_range( FILE* const f,
  int const M, int const N, int const LD,
  int* row, int* col, int const n_entries,
  double* const val )
{
  int idx = (*col) * LD + (*row);

  for ( int n_read = 0; n_read < n_entries; ++n_read ) { 
    int const read = fscanf( f, "%lg %lg", &val[2*idx], &val[2*idx+1]);

    get_next_entry( LD, col, row, &idx );

    if ( read !=  2 )
      return MM_PREMATURE_EOF;
  }

  return 0;
}

int mm_write_mtx_crd_real_data( FILE* const f,
  int const nz,
  int const* const I, int const* const J, double const* const val )
{
  int const idx_start = 0;
  int const n_entries = nz;

  int const ierr = mm_write_mtx_crd_real_data_range( f,
    idx_start, n_entries,
    I, J, val );

  return ierr;
}

int mm_write_mtx_crd_complex_data( FILE* const f,
  int const nz,
  int const* const I, int const* const J, double const* const val )
{
  int const idx_start = 0;
  int const n_entries = nz;

  int const ierr = mm_write_mtx_crd_complex_data_range( f,
    idx_start, n_entries,
    I, J, val );

  return ierr;
}

int mm_write_mtx_crd_pattern_data( FILE* const f,
  int const nz,
  int const* const I, int const* const J )
{
  int const idx_start = 0;
  int const n_entries = nz;

  int const ierr = mm_write_mtx_crd_pattern_data_range( f,
    idx_start, n_entries,
    I, J );

  return ierr;
}

int mm_write_mtx_arr_real_data( FILE* const f,
  int const M, int const N, int const LD,
  double const* const val )
{
  int row = 0;
  int col = 0;
  int const n_entries = M*N;

  int const ierr = mm_write_mtx_arr_real_data_range( f,
    M, N, LD,
    &row, &col, n_entries,
    val );

  return ierr;
}

int mm_write_mtx_arr_complex_data( FILE* const f,
  int const M, int const N, int const LD,
  double const* const val )
{
  int row = 0;
  int col = 0;
  int const n_entries = M*N;

  int const ierr = mm_write_mtx_arr_complex_data_range( f,
    M, N, LD,
    &row, &col, n_entries,
    val );

  return ierr;
}

int mm_write_mtx_crd_real_data_range( FILE* const f,
  int const idx_start, int const n_entries,
  int const* const I, int const* const J, double const* const val )
{
  int const idx_end = idx_start + n_entries;

  for ( int i = idx_start; i < idx_end; ++i ) {
    int const I_i = I[i] + 1;
    int const J_i = J[i] + 1;
    fprintf( f, "%d %d %20.16g\n", I_i, J_i, val[i] );
  }

  return 0;
}

int mm_write_mtx_crd_complex_data_range( FILE* const f,
  int const idx_start, int const n_entries,
  int const* const I, int const* const J, double const* const val )
{
  int const idx_end = idx_start + n_entries;

  for ( int i = idx_start; i < idx_end; ++i ) {
    int const I_i = I[i] + 1;
    int const J_i = J[i] + 1;
    fprintf( f, "%d %d %20.16g %20.16g\n", I_i, J_i, val[2*i], val[2*i+1] );
  }

  return 0;
}

int mm_write_mtx_crd_pattern_data_range( FILE* const f,
  int const idx_start, int const n_entries,
  int const* const I, int const* const J )
{
  int const idx_end = idx_start + n_entries;

  for ( int i = idx_start; i < idx_end; ++i ) {
    int const I_i = I[i] + 1;
    int const J_i = J[i] + 1;
    fprintf( f, "%d %d\n", I_i, J_i );
  }

  return 0;
}

int mm_write_mtx_arr_real_data_range( FILE* const f,
  int const M, int const N, int const LD,
  int* row, int* col, int const n_entries,
  double const* const val )
{
  int idx = (*col) * LD + (*row);

  for ( int n_read = 0; n_read < n_entries; ++n_read ) {
    fprintf( f, "%20.16g\n", val[idx] );

    get_next_entry( LD, col, row, &idx );
  }

  return 0;
}

int mm_write_mtx_arr_complex_data_range( FILE* const f,
  int const M, int const N, int const LD,
  int* row, int* col, int const n_entries,
  double const* const val )
{
  int idx = (*col) * LD + (*row);

  for ( int n_read = 0; n_read < n_entries; ++n_read ) {
    fprintf( f, "%20.16g %20.16g\n", val[2*idx], val[2*idx+1] );

    get_next_entry( LD, col, row, &idx );
  }

  return 0;
}

/*****************************************************************************/
/* Auxiliary functions                                                       */
/*****************************************************************************/

/**
*  Create a new copy of a string s. mm_strdup() is a common routine, but
*  not part of ANSI C, so it is included here. Used by mm_typecode_to_str().
*
*/
static char* mm_strdup( const char* s )
{
  int len = strlen( s );
  char *s2 = (char*) malloc( (len + 1) * sizeof(char) );
  return strcpy(s2, s);
}

char* mm_typecode_to_str( MM_typecode const matcode )
{
  char buffer[MM_MAX_LINE_LENGTH];
  char *types[4];
  int error = 0;

  /* check for MTX type */
  if ( mm_is_matrix( matcode ) )
    types[0] = MM_MTX_STR;
  else
    error = 1;

  /* check for CRD or ARR matrix */
  if ( mm_is_sparse( matcode ) )
    types[1] = MM_SPARSE_STR;
  else if ( mm_is_dense( matcode ) )
    types[1] = MM_DENSE_STR;
  else
    return NULL;

  /* check for element data type */
  if ( mm_is_real( matcode ) )
    types[2] = MM_REAL_STR;
  else if ( mm_is_complex( matcode ) )
    types[2] = MM_COMPLEX_STR;
  else if ( mm_is_pattern( matcode ) )
    types[2] = MM_PATTERN_STR;
  else if ( mm_is_integer( matcode ) )
    types[2] = MM_INT_STR;
  else
    return NULL;

  /* check for symmetry type */
  if ( mm_is_general( matcode ) )
    types[3] = MM_GENERAL_STR;
  else if ( mm_is_symmetric( matcode ) )
    types[3] = MM_SYMM_STR;
  else if ( mm_is_hermitian( matcode ) )
    types[3] = MM_HERM_STR;
  else if ( mm_is_skew( matcode ) )
    types[3] = MM_SKEW_STR;
  else
    return NULL;

  sprintf( buffer,"%s %s %s %s", types[0], types[1], types[2], types[3] );

  return mm_strdup( buffer );
}

inline void get_next_entry( int const LD, int* col, int* row, int* idx )
{
  *row += 1;

  if ( *row >= LD ) {
    *col += 1;
    *row = 0;
    *idx = (*col) * LD;
  } else {
    *idx += 1; 
  }
}
